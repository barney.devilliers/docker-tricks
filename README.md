# docker-tricks

## updating the Gem lockfile of a project without having Ruby installed:
```bash
docker run --rm --user=$UID -v $PWD:/src ruby:2.6.2 /bin/bash -c 'cd /src; ls -la; bundle lock --update'
```

## Must have aliases added to my `~/.bashrc` file
```bash
alias dc='docker-compose'
alias dc-wipe-images='docker rmi $(docker images -a -q) -f'
alias dc-wipe-all='docker rm $(docker ps -a -q) -f; docker system prune --volumes -f'
alias dc-wipe-this='docker-compose kill; docker-compose down --volumes; docker system prune --volumes -f'
```

## Fixing inevitable ownership issues in a git folder:
```bash
sudo chown $UID:$UID . -R
```

## Running ruby irb quickly
```bash
 docker run --rm -it -v$PWD:/src ruby:2.6.2 /bin/bash -c 'cd /src; irb'
```

## Get into image without predefined entrypoints or command
```bash
docker run --rm -it --entrypoint='' theimage sh
```

## Analytics

```bash
docker ps -q | xargs  docker stats --no-stream
```